﻿using DB;

// An example of how to declare and use a Fluent interface.

// Instead of saying we want an InMemoryQueryBuilder we are referencing
// the actual abstraction here (QueryBuilder). That means when we do the
// actual query below we can only call the members of the interface.
QueryBuilder builder = new InMemoryQueryBuilder(50);

// An alternative could be:
// QueryBuilder builder = new DatabaseQueryBuilder(table: "records");

// If we wanted to use a different kind of QueryBuilder we could swap it
// out and the code below would remain functional.
List<Record> entries = builder
    .Contains("X")
    .Sort(Ascending: true)
    .GetAll();

foreach (var entry in entries) {
    Console.WriteLine(entry.Value);
}