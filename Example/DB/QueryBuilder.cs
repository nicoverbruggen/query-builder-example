﻿using System;
using System.Collections.Generic;
using App;

namespace DB
{
    public interface QueryBuilder
    {
        // Operations
        public QueryBuilder Contains(string Query);
        public QueryBuilder Sort(bool Ascending);

        // Retrieval
        public Record GetFirst();
        public List<Record> GetAll();
    }
}

