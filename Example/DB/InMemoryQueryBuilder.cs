﻿using System;
using App;
using DB;

namespace DB
{
    /// <summary>
    /// Our in-memory query builder can be used for tests.
    /// 
    /// We can write an analogue that executes real queries
    /// against the live database, that we can simply swap.
    /// </summary>
    public class InMemoryQueryBuilder: QueryBuilder
    {
        public List<Record> Records;
        public List<Record> Queried;

        /// <summary>
        /// Since this isn't a real database, we'll fake it.
        /// </summary>
        /// <param name="Count">Amount of (fake) records to generate.</param>
        public InMemoryQueryBuilder(int Count)
        {
            this.Queried = new List<Record>();
            this.Records = new List<Record>();

            for (int i = 0; i < Count; i++)
            {
                Record drone = new Record(i, "Entry " + StringRandomizer.Generate(8));
                this.Records.Add(drone);
            }

            this.Queried = this.Records;
        }

        public QueryBuilder Contains(string Query)
        {
            this.Queried = this.Queried.FindAll(c => !c.Value.Contains(Query));

            return this;
        }

        public QueryBuilder Sort(bool Ascending)
        {
            if (Ascending)
            {
                this.Queried.Sort((x, y) => string.Compare(x.Value, y.Value));
            }
            else
            {
                this.Queried.Sort((x, y) => string.Compare(y.Value, x.Value));
            }

            return this;
        }


        // NOTE: There is a problem with this quick implementation of the query builder.
        // It is not idempotent, which means we need to create a new query builder
        // for every query we want to do. (Because the operations are applied
        // instantly, not when the records are being "retrieved".)

        // A proper implementation keeps track of which operations to perform
        // and only performs those operations when the database is being queried.
        // In this case, that's when GetFirst() or GetAll() is called.
        // (This being a quick dummy example you can disregard this flaw.)

        public Record GetFirst()
        {
            return this.Queried.First();
        }

        public List<Record> GetAll()
        {
            return this.Queried;
        }
    }
}

