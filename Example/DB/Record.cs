﻿using System;

namespace DB
{
    /// <summary>
    /// Dummy record.
    /// </summary>
    public struct Record
    {
        public int Identifier;
        public string Value;

        public Record(int Identifier, string Value)
        {
            this.Identifier = Identifier;
            this.Value = Value;
        }
    }
}

